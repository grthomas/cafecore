#!/usr/bin/python
########################################################################################
#Part of the gene family clustering pipeline. This script takes an MCL dump
#output file and converts it into a properly formatted CAFE input file.
#The CAFE filtering should still be filtered with cafe_filter.py.
#
#Command line options:
#	-i: Input MCL dump file
#	-l: A space delimited list of unique species identifiers (ie for ENSEMBL speces: "ENSFCA ENSCAF ENSAME ... ")
#	-o: The output file in CAFE format
#
#Sample usage: python mcl_to_cafe.py -i dump.mcl_dump_file -l "ENSFCA ENSCAF ENSAME ENSMPU ENSBTA ENSSSC ENSECA ENSP00 ENSLAF" -o cafe_formatted_output_file.tab
#
#Gregg Thomas, August 2013
########################################################################################

import sys, re, os, argparse
sys.path.append(sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../corelib/"))
import core

############################################
#Function Definitions
############################################


def IO_fileParse():
#This function handles the command line options.

	parser = argparse.ArgumentParser();

	parser.add_argument("-i", dest="input_file", help="The MCL dump file.");
	parser.add_argument("-l", dest="spec_labels", help="Each species should have a unique identifier in their gene IDs. Enter them here as a space delimited list in quotes (ie \"ENSFCA,ENSCAF\")");
	parser.add_argument("-o", dest="output_file", help="Desired output file name.");

	args = parser.parse_args();

	if args.input_file == None or args.spec_labels == None or args.output_file == None:
		parser.print_help();
		sys.exit();

	return args.input_file, args.spec_labels, args.output_file;

############################################
#Main Block
############################################

inFilename, specLabels, outFilename = IO_fileParse();

print "=======================================================================";
print "Converting file:\t" + inFilename;
print "Using species labels:\t" + specLabels;
print "Writting output to:\t" + outFilename;
print "--------------------------";
print "Converting MCL output to CAFE input...";

specLabels = specLabels.split();

outFile = open(outFilename, "w");
outFile.write("Desc\tFamily ID\t");

for x in xrange(len(specLabels)):
	outFile.write(specLabels[x]);

	if x == len(specLabels)-1:
		outFile.write("\n");
	else:
		outFile.write("\t");

specDict = {};

i = 0;
rotator = 0;

for line in open(inFilename, "r"):

	rotator = core.loadingRotator(i, rotator, 100)
	i = i + 1;

	if line.find("Warning") != -1 or line.find("(U)") != -1:
		continue;

	for key in specLabels:
		specDict[key] = 0;

	tmpline = line.split("\t");

	for each in tmpline:
		for label in specLabels:
			if each.find(label) != -1:
				curkey = label;

		specDict[curkey] = specDict[curkey] + 1;

	outline = "(null)\t" + str(i) + "\t";
	outFile.write(outline);

	for y in xrange(len(specLabels)):
		writespec = specLabels[y];
		outFile.write(str(specDict[writespec]));

		if y == len(specLabels)-1:
			outFile.write("\n");
		else:
			outFile.write("\t");

sys.stderr.write('\b');
print "Done!";
outFile.close();
print "=======================================================================";


