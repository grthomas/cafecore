#!/usr/bin/python
########################################################################################
#Script to filter out isoforms from peptide files in FASTA ENSEMBL or NCBI format. This
#script can also add an easier to read species label to each sequence within the file.
#
#Sample ENSEMBL usage: python isoform_filter.py -i [input_fasta_file] -t ens -l [species_label] -o [output_filename]
#
#Sample NCBI usage: python isoform_filter.py -i [input_fasta_file] -t ncbi -g [toplevel_gff_file] -l [species_label] -o [output_filename]
#
#To just do the relabeling, set -f 0. You shouldn't need a gff file for the NCBI file in
#this case. For NCBI relabeling, the gene ID is also moved to the front of the title line.
#
#Written by: Gregg Thomas, Summer 2014
#
#NCBI filter command kindly provided by the folks at NCBI.
#
########################################################################################

import sys, re, os, argparse
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../corelib/")
import core

############################################
#Function definitions.

def IO_fileParse():
#This function handles the command line options.

	parser = argparse.ArgumentParser()

	parser.add_argument("-i", dest="input_file", help="An input file containing peptides from a species in FASTA format");
	parser.add_argument("-t", dest="file_type", help="Currently supported file types are ENSEMBL and NCBI peptide files. Enter as 'ens' or 'ncbi' here. Note: If file type is NCBI you will also need to specify the top level gff file with -g")
	parser.add_argument("-g", dest="gff_file", help="If file type is NCBI, the top level gff file is also needed and should be specified here.");
	parser.add_argument("-f", dest="filter_flag", help="Boolean which tells the script to perform filtering (1) or not (0). Default: 1", type=int, default=1);
	parser.add_argument("-l", dest="spec_label", help="A species label to add to the gene ID of each sequence.", default="");
	parser.add_argument("-o", dest="output_file", help="OPTIONAL: The desired name of the output file. If none is specified the default is [input_filename]_isofiltered.fa or [input_filename]_isofiltered_relabel.fa");

	args = parser.parse_args();

	if args.input_file == None or args.file_type == None:
		parser.print_help();
		sys.exit();

	if args.file_type == "ens" and args.gff_file != None:
		print " ----------------------------------------------------------------------";
		print "|**Error 1: A gff file (-g) should not be specified with file type ens |";
		print " ----------------------------------------------------------------------";
		sys.exit();

	if args.file_type == "ncbi" and args.filter_flag == 0 and args.gff_file == None:
		print " -----------------------------------------------------------------";
		print "|**Error 2: A gff file (-g) must be specified with file type ncbi |";
		print " -----------------------------------------------------------------";
		sys.exit();

	if args.filter_flag not in [0,1]:
		print " ------------------------------------------------";
		print "|**Error 3: -f must take values of either 0 or 1 |";
		print " ------------------------------------------------";
		sys.exit();

	if args.filter_flag == 0 and args.spec_label == "":
		print " ---------------------------------------------------------------------------------------------------------------";
		print "|**Error 4: Neither -f 1 or -l specified. At least one must be specified to perform one of the scripts actions. |";
		print " ---------------------------------------------------------------------------------------------------------------";
		sys.exit();

	if args.output_file == None and args.spec_label == None:
		args.output_file = args.input_file[:args.input_file.index(".fa")] + "_isofiltered.fa";
	elif args.output_file == None and args.spec_label != None:
		args.output_file = args.input_file[:args.input_file.index(".fa")] + "_isofiltered_relabel.fa";

	return args.input_file, args.file_type, args.gff_file, args.filter_flag, args.spec_label, args.output_file;

############################################
def ensFilter(inSeqs, filterflag, speclabel, outFilename):

	rotator = 0;
	numbars = 0;
	donepercent = [];
	i = 0;

	if filterflag == 1:
		print "Indexing", len(inSeqs), "sequences to be filtered.";
		print "Parsing identifiers...";

		for each in inSeqs:

			rotator = core.loadingRotator(i, rotator, 100)

			curTitle, curSeq = core.getFastafromInd(inFilename, each[0], each[1], each[2], each[3]);

			geneid = curTitle[curTitle.index("gene:") + 5:curTitle.index("gene:") + 23];

			if geneid in identDict:
				identDict[geneid].append(each);

			else:
				identDict[geneid] = [];
				identDict[geneid].append(each);

			i = i + 1;

		sys.stderr.write('\b');

		print "Filtering and writing sequences...";

		i = 0;
		count = 0;

		for key in identDict:

			numbars, donepercent = core.loadingBar(i, len(identDict), donepercent, numbars);

			if len(identDict[key]) == 1:
				curTitle, curSeq = core.getFastafromInd(inFilename, identDict[key][0][0], identDict[key][0][1], identDict[key][0][2], identDict[key][0][3]);

				if speclabel != "":
					newTitle = ">" + speclabel + "_" + curTitle[1:];
					core.writeSeq(outFilename, curSeq, newTitle);
				else:
					core.writeSeq(outFilename, curSeq, curTitle);

				count = count + 1;

			else:
				titlelist = [];
				seqlist = [];

				for inds in identDict[key]:
					aTitle, aSeq = core.getFastafromInd(inFilename, inds[0], inds[1], inds[2], inds[3]);

					titlelist.append(aTitle);
					seqlist.append(aSeq);

				longseq = max(seqlist, key=len)

				for inds in identDict[key]:
					aTitle, aSeq = core.getFastafromInd(inFilename, inds[0], inds[1], inds[2], inds[3]);

					if aSeq == longseq:
						curTitle, curSeq = core.getFastafromInd(inFilename, inds[0], inds[1], inds[2], inds[3]);

						if speclabel != "":
							newTitle = ">" + speclabel + "_" + curTitle[1:];
							core.writeSeq(outFilename, curSeq, newTitle);
						else:
							core.writeSeq(outFilename, curSeq, curTitle);

						count = count + 1;
						break;

			i = i + 1;

		pstring = "100.0% complete.";
		sys.stderr.write('\b' * len(pstring) + pstring);
		print "\nDone!";
		print count, "out of", len(identDict), "identifiers written.";
		print len(inSeqs) - count, "sequences filtered.";

	else:
		print "Relabeling...";
		for seq in inSeqs:
	
			numbars, donepercent = core.loadingBar(i, len(inSeqs), donepercent, numbars);
			i = i + 1;

			curTitle, curSeq = core.getFastafromInd(inFilename, seq[0], seq[1], seq[2], seq[3]);

			newTitle = ">" + speclabel + "_" + curTitle[1:];

			core.writeSeq(outFilename, curSeq, newTitle);


		pstring = "100.0% complete.";
		sys.stderr.write('\b' * len(pstring) + pstring);
		print "\nDone!";

############################################
def ncbiFilter(inSeqs,filterflag,gffFilename,speclabel,outFilename):

	numbars = 0;
	donepercent = [];
	i = 0;

	if filterflag == 1:
		print "Obtaining longest isoforms from .gff file...";

		cmd = "zcat " + gffFilename + " | awk \'BEGIN{FS=\"	\";OFS=\"|\"}$3==\"CDS\"{if($4<$5){print $5-$4+1,$9}else{print $4-$5+1,$9}}\' | grep \"[NX]P[_]\" | sed \'s/\([0-9]*\).*GeneID:\([0-9]*\).*\([NX]P[_][0-9]*\.[0-9]*\).*/\\1|\\2|\\3/\' | awk \'BEGIN{FS=\"|\";OFS=\"\t\";gene=\"\";acc=\"\";len=0}{if(acc!=$3){print gene,acc,len/3-1;gene=$2;acc=$3;len=$1}else{len=len+$1}}END{print gene,acc,len/3-1}\' | sort -k1,1n -k3,3nr -k2,2 | awk \'BEGIN{FS=\"	\";OFS=\"	\";gene=\"\";acc=\"\";len=0}{if(gene!=$1){print $1,$2,$3};gene=$1;acc=$2;len=$3}\' > ncbi_isoform_filter_tmp11567.txt"

		os.system(cmd);

		tmpFile = open("ncbi_isoform_filter_tmp11567.txt", "r");
		tmpLines = tmpFile.readlines();
		tmpFile.close();

		os.system("rm ncbi_isoform_filter_tmp11567.txt");

		longest_isos = [];

		for each in tmpLines:
			longest_isos.append(each.split("\t")[1]);
		longest_isos = filter(None, longest_isos);

		print "Writing longest isoforms to output file...";

		count = 0;

		for each in inSeqs:
			numbars, donepercent = core.loadingBar(i, len(inSeqs), donepercent, numbars);
			i = i + 1;

			curTitle, curSeq = core.getFastafromInd(inFilename, each[0], each[1], each[2], each[3]);

			found = 0;

			for gid in longest_isos:
				if gid in curTitle:
					found = 1;
					break;

			if found == 1:
				if speclabel != "":
					gid = curTitle[curTitle.index("P_")-1:curTitle.index("|",curTitle.index("P_"))]
					newTitle = ">" + speclabel + "_" + gid + " |" + curTitle[1:curTitle.index("P_")-1] + curTitle[curTitle.index("|",curTitle.index("P_"))+1:];
					core.writeSeq(outFilename, curSeq, newTitle);
				else:
					core.writeSeq(outFilename, curSeq, curTitle);
				count = count + 1;

		pstring = "100.0% complete.";
		sys.stderr.write('\b' * len(pstring) + pstring);
		print "\nDone!";
		print count, "out of", len(inSeqs), "identifiers written.";
		print len(inSeqs) - count, "sequences filtered.";

	else:
		print "Relabeling...";
		for seq in inSeqs:
	
			numbars, donepercent = core.loadingBar(i, len(inSeqs), donepercent, numbars);
			i = i + 1;

			curTitle, curSeq = core.getFastafromInd(inFilename, seq[0], seq[1], seq[2], seq[3]);

			gid = curTitle[curTitle.index("P_")-1:curTitle.index("|",curTitle.index("P_"))]
			newTitle = ">" + speclabel + "_" + gid + " |" + curTitle[1:curTitle.index("P_")-1] + curTitle[curTitle.index("|",curTitle.index("P_"))+1:];

			core.writeSeq(outFilename, curSeq, newTitle);

		pstring = "100.0% complete.";
		sys.stderr.write('\b' * len(pstring) + pstring);
		print "\nDone!";


############################################
#Main Block
############################################

inFilename, fType, gfff, fFlag, specID, ofilename = IO_fileParse();

print "=======================================================================";
print "Filtering isoforms from:\t\t" + inFilename;
if fType == "ens":
	print "Input file type:\t\t\tEnsembl";
elif fType == "ncbi":
	print "Input file type:\t\t\tNCBI";
	print "Using GFF file:\t\t\t\t" + gfff;
if specID != "":
	print "Adding species label to gene ID:\t" + specID;
if fFlag == 0:
	print "-f set to 0:\t\t\t\tNOT performing filtering. Just relabeling.";
print "Writing output to:\t\t\t" + ofilename;

outFile = open(ofilename, "w");
outFile.write("");
outFile.close();

identDict = {};

print "--------------------------";

ins = core.fastaGetFileInd(inFilename);

if fType == "ens":
	ensFilter(ins,fFlag,specID,ofilename)
elif fType == "ncbi":
	ncbiFilter(ins,fFlag,gfff,specID,ofilename);

print "=======================================================================";

