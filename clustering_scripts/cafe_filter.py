#!/usr/bin/python
########################################################################################
#This script is part of the gene family clustering pipeline. It takes a CAFE
#input file and filters it according to the user input species groupings and/or family
#size.
#
#The entire list is contained in quotes. Groups are separated by commas and
#species are separated by spaces.
#
#Sample usage: python cafe_filter.py -i Carn_9spec_I30.tab -g "Panda Ferret Dog Cat Cow Pig, Horse Human Elephant" -s Cat -o Carn_9spec_filtered.tab
#The above usage will filter by both the groupings specified in -g and by size (any species with a family > 100 will be filtered). Adjust -d and -f
#to change filtering options.
#
#Gregg Thomas, August 2013
########################################################################################

import sys, re, os, argparse
sys.path.append(sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../corelib/"))
import core

############################################
#Function Definitions
############################################


def IO_fileParse():
#This function handles the command line options.

	parser = argparse.ArgumentParser();

	parser.add_argument("-i", dest="input_file", help="The unfiltered CAFE input file.");
	parser.add_argument("-d", dest="group_filter", help="Boolean option to specify whether to filter by groups (1) or not (0). Default: 1", type=int, default=1);
	parser.add_argument("-g", dest="group_labels", help="Species labels as a space delimited list with groups separated by commas to indicate filtering groups. Default: All species are one group", default="");
	parser.add_argument("-f", dest="size_filter", help="Boolean option to specify whether to filter by gene family size (1) or not (0). Default: 1", type=int, default=1);
	parser.add_argument("-o", dest="output_file", help="Output file name for filtered gene families.");
	parser.add_argument("-s", dest="spec_o_interest", help="OPTIONAL: To count the number of genes and families filtered from a particular species, enter its species label with this option.");

	args = parser.parse_args();

	if args.input_file == None or args.output_file == None:
		parser.print_help();
		sys.exit();

	if args.group_filter not in [0,1]:
		print " ------------------------------------------------";
		print "|**Error 1: -d must take values of either 0 or 1 |";
		print " ------------------------------------------------";
		parser.print_help();
		sys.exit();

	if args.size_filter not in [0,1]:
		print " ------------------------------------------------";
		print "|**Error 2: -f must take values of either 0 or 1 |";
		print " ------------------------------------------------";
		parser.print_help();
		sys.exit();


	return args.input_file, args.group_filter, args.group_labels, args.size_filter, args.spec_o_interest, args.output_file;

############################################
#Main Block
############################################

inFilename, groupFilter, inGroups, sizeFilter, specofinterest, outFilename = IO_fileParse();

print "=======================================================================";
print "Filtering file:\t\t\t" + inFilename;
print "Writing output to:\t\t" + outFilename;

inFile = open(inFilename, "r");
inLines = inFile.readlines();
inFile.close();

firstline = inLines[0].replace("\n", "").split("\t");
firstline.pop(0);
firstline.pop(0);

if inGroups == "":
	for x in xrange(len(firstline)):
		if x != len(firstline) - 1:
			inGroups = inGroups + firstline[x] + " ";
		else:
			inGroups = inGroups + firstline[x];
#This block prepares the group based on the first line if -g was left default.

if groupFilter == 1 and sizeFilter == 0:
	print "Filtering by groupings only.";
	groups = inGroups.split(", ");
	print "Based on the input groupings, for each family at least 2 species in each\nof the following groups must contain a gene, else it is filtered out:";
	for x in xrange(len(groups)):
		outline = "Group " + str(x+1) + ":\t" + groups[x];
		print outline;
if groupFilter == 0 and sizeFilter == 1:
	print "Filtering by size only.";
	largefamFile = outFilename + "_large_fams.tab";
	print "Moving large families to:\t" + largefamFile;
if groupFilter == 1 and sizeFilter == 1:
	print "Filtering by both groupings and size.";
	largefamFile = outFilename + "_large_fams.tab";
	print "Moving large families to:\t" + largefamFile;
	groups = inGroups.split(", ");
	print "Based on the input groupings, for each family at least 2 species in each\nof the following groups must contain a gene, else it is filtered out:";
	for x in xrange(len(groups)):
		outline = "Group " + str(x+1) + ":\t" + groups[x];
		print outline;
if groupFilter == 0 and sizeFilter == 0:
	print " ----------------------------------------------------------------";
	print "|Warning: Both filter options set to 0. No filtering will occur! |";
	print " ----------------------------------------------------------------";
#Some nice screen output based on the options.

specCheck = inGroups.replace(",","").split(" ");
notin = [];
for each in specCheck:
	if each not in firstline:
		notin.append(each);
if notin != []:
	errline = "|**Error 3: The following labels were not found in the input file: ";
	for x in xrange(len(notin)):
		if x != len(notin) - 1:
			errline = errline + notin[x] + ", ";
		else:
			errline = errline + notin[x] + " |";
	print " " + "-" * (len(errline)-2);
	print errline;
	print " " + "-" * (len(errline)-2);
	sys.exit();
#This checks to make sure all labels input are actually in the input file.

if specofinterest != None:
	found = 0;
	for n in xrange(len(firstline)):
		if firstline[n] == specofinterest:
			specofinterest_index = n;
			found = 1;

	if found == 0:
		print " ---------------------------------------------------------------------------";
		print "|**Error 4: Species of interest not found in input file. Check your labels. |"
		print " ---------------------------------------------------------------------------";
		sys.exit();
#This makes sure the species of interest is found in the input file.

print "--------------------------";
print "Counting number of genes and families in input file...";

p = 0;
genecount = 0;
intcount = 0;

for line in inLines:
	if p == 0:
		p = p + 1;
		continue;

	cline = line.replace("\n", "").split("\t");
	cline.pop(0);
	cline.pop(0);
	
	counter = 0;
	for specgenes in cline:
		genecount = genecount + int(specgenes);
		if specofinterest != None:
			if counter == specofinterest_index:
				intcount = intcount + int(specgenes);
		counter = counter + 1;
	p = p + 1;
numFams = len(inLines) - 1;
#This loop counts the number of genes in the input before filtering.

if sizeFilter == 1 and groupFilter == 1:
	print "Filtering " + str(numFams) + " families (" + str(genecount) + " genes) based on input groupings and family sizes...";
elif sizeFilter == 1 and groupFilter == 0:
	print "Filtering " + str(numFams) + " families (" + str(genecount) + " genes) based on family sizes...";
elif sizeFilter == 0 and groupFilter == 1:
	print "Filtering " + str(numFams) + " families (" + str(genecount) + " genes) based on input groupings...";
elif sizeFilter == 0 and groupFilter == 0:
	print "Performing no filtering on " + str(numFams) + " families (" + str(genecount) + " genes)...";
if specofinterest != None:
	print specofinterest +  ": " + str(intcount) + " genes.";

groupLists = inGroups.split(',');
for y in xrange(len(groupLists)):
	groupLists[y] = groupLists[y].split(" ");
	groupLists[y] = filter(None, groupLists[y]);

savedList = groupLists;
numfiltered = 0;
numskipped = 0;
genefiltered = 0;
geneskipped = 0;
intskipped = 0;
intfiltered = 0;
#Variables for counting various things.

outFile = open(outFilename, "w");
if sizeFilter == 1:
	lFile = open(largefamFile, "w");

i = 0;
rotator = 0;

for x in xrange(len(inLines)):

	rotator = core.loadingRotator(i, rotator, 100);

	i = i + 1;

	if x == 0:
		outFile.write(inLines[x]);
		if sizeFilter == 1:
			lFile.write(inLines[x]);

		specs = inLines[x].replace("\n","").replace("\r","").split("\t")[2:];

		for z in xrange(len(specs)):
			for a in xrange(len(groupLists)):
				if specs[z] in groupLists[a]:
					for b in xrange(len(groupLists[a])):
						if groupLists[a][b] == specs[z]:
							groupLists[a][b] = z+2;

	else:
		newline = inLines[x].replace("\n","").replace("\r","").split("\t");
		famsizes = [];

		###
		#Begin size filter.
		if sizeFilter == 1:
			flag = 0;
			for a in xrange(2,len(newline)):
				if int(newline[a]) > 100:
					flag = 1;

			if flag == 1:
				numskipped = numskipped + 1;
				lFile.write(inLines[x]);

				cline = newline;
				cline.pop(0);
				cline.pop(0);

				counter = 0;
				for specgenes in cline:
					geneskipped = geneskipped + int(specgenes);
					if specofinterest != None:
						if counter == specofinterest_index:
							intskipped = intskipped + int(specgenes);
					counter = counter + 1;
				continue;
		#End size filter.
		###
		#Begin group filter.
		if groupFilter == 1:
			if (len(groupLists)) == 1:
				if newline.count('0') < len(groupLists[0]) - 1:
					writeline = "\t".join(newline);
					outFile.write(writeline);
					outFile.write("\n");

				else:
					numfiltered = numfiltered + 1;

					cline = newline;
					cline.pop(0);
					cline.pop(0);

					counter = 0;
					for specgenes in cline:
						genefiltered = genefiltered + int(specgenes);
						if specofinterest != None:
							if counter == specofinterest_index:
								intfiltered = intfiltered + int(specgenes);
						counter = counter + 1;

			else:
				groupsum = 0;
				for each in groupLists:
					famsum = 0;
					for column in each:
						famsum = famsum + int(newline[column]);
					if famsum != 0:
						groupsum = groupsum + 1;

				if groupsum == len(groupLists):
					writeline = "\t".join(newline);
					outFile.write(writeline);
					outFile.write("\n");

				else:
					numfiltered = numfiltered + 1;

					cline = newline;
					cline.pop(0);
					cline.pop(0);

					counter = 0;
					for specgenes in cline:
						genefiltered = genefiltered + int(specgenes);

						if specofinterest != None:
							if counter == specofinterest_index:
								intfiltered = intfiltered + int(specgenes);
						counter = counter + 1;
			#End group filter
			###

		else:
			writeline = "\t".join(newline);
			outFile.write(writeline);
			outFile.write("\n");

outFile.close();
if sizeFilter == 1:
	lFile.close();

sys.stderr.write('\b');
print "--------------------------";
if sizeFilter == 1 and groupFilter == 1:
	print "Done! Here's some info:\nFiltered out " + str(numfiltered + numskipped) + " total families. " + str(numfiltered) + " families based on input groupings and " + str(numskipped) + " based on family size."
	print "Filtered out " + str(genefiltered + geneskipped) + " total genes. " + str(genefiltered) + " genes based on input groupings and " + str(geneskipped) + " based on family size."
	if specofinterest != None:
		print specofinterest + ": Filtered out " + str(intfiltered + intskipped) + " total genes. " + str(intfiltered) + " genes based on input groupings and " + str(intskipped) + " based on family size."

elif sizeFilter == 1 and groupFilter == 0:
	print "Done! Here's some info:\nFiltered out " + str(numskipped) + " families based on family sizes."
	print "Filtered out " + str(geneskipped) + " genes based on family sizes."
	if specofinterest != None:
		print specofinterest + ": Filtered out " + str(intfiltered + intskipped) + " genes based on family sizes."

elif sizeFilter == 0 and groupFilter == 1:
	print "Done! Here's some info:\nFiltered out " + str(numfiltered) + " families based on input groupings."
	print "Filtered out " + str(genefiltered) + " genes based on input groupings."
	if specofinterest != None:
		print specofinterest + ": Filtered out " + str(intfiltered + intskipped) + " genes based on input groupings."

elif sizeFilter == 0 and groupFilter == 0:
	print "Done! Here's some info:\nFiltered out " + str(numfiltered) + " families."
	print "Filtered out " + str(genefiltered) + " genes."
	if specofinterest != None:
		print specofinterest + ": Filtered out " + str(intfiltered + intskipped) + " genes."
print "=======================================================================";



