#!/usr/bin/python
########################################################################################
#Included in the gene family clustering pipeline. This is a simple script
#to combine multiple multi-fasta files into one large fasta file.
#
#The input directory should contain only the fasta files you wish to combine.
#Sample usage: python fasta_combine.py -d input_directory -o output_file
#
#Gregg Thomas, August 2013
########################################################################################

import sys, os, argparse
sys.path.append(sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../corelib/"))
import core

############################################
#Function definitions.

def IO_fileParse():
#This function handles the command line options.

	parser = argparse.ArgumentParser()

	parser.add_argument("-d", dest="input_directory", help="An input directory containing multiple files in FASTA format.");
	parser.add_argument("-o", dest="output_file", help="The desired name of the output file.");

	args = parser.parse_args();

	if args.output_file == None:
		parser.print_help();
		sys.exit();

	if args.input_directory == None:
		args.input_directory = "";

	return args.input_directory, args.output_file;

############################################
#Main Block
############################################

inDir, outFilename = IO_fileParse();

remnonf = 0;
if inDir == "":
	inDir = os.getcwd();
	remnonf = 1;

if inDir[len(inDir)-1:] != "/":
	inDir = inDir + "/";

print "=======================================================================";
if remnonf == 0:
	print "Combining all .fa files in:\t\t\t" + inDir;
elif remnonf == 1:
	print "Combining all _isofiltered.fa files in:\t\t" + inDir;
print "Writing output to:\t\t\t\t" + outFilename;

if inDir[len(inDir)-1] != "/":
	inDir = inDir + "/";

fileList = os.listdir(inDir);

outFile = open(outFilename, "w");
outFile.write("");
outFile.close();

if remnonf == 1:
	newfileList = [];
	for x in xrange(len(fileList)):
		if fileList[x].find("_isofiltered") != -1:
			newfileList.append(fileList[x]);
	fileList = newfileList;

i = 0;

for each in fileList:

	if each.find(".fa") == -1:
		continue;

	if i != 0:
		print "\nProceeding to next file...";

	i = i + 1;

	print "------ Loading file " + str(i) + ": " + each;

	infilename = inDir + each;
	inSeqs = core.fastaGetFileInd(infilename)

	numbars = 0;
	donepercent = [];

	k = 0;

	for seq in inSeqs:

		numbars, donepercent = core.loadingBar(k, len(inSeqs), donepercent, numbars);
		k = k + 1;

		curTitle, curSeq = core.getFastafromInd(infilename, seq[0], seq[1], seq[2], seq[3]);
		core.writeSeq(outFilename, curSeq, curTitle)

	pstring = "100.0% complete.";
	sys.stderr.write('\b' * len(pstring) + pstring);


print "\nDone!";
print "=======================================================================";
