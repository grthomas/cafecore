#!/usr/bin/python
#############################################################################
#Genome Assembly and Annotation Error Estimation Algorithm Utilizing CAFEv3.0
#Written by: Gregg Thomas, Summer 2012
#Hahn Lab, Indiana University
#Contact: grthomas@indiana.edu
#
#This version of the script first minimizes scores for varying error models across all species 
#and then (if specified) incrementally adds or subtracts 10% of the global error to each species 
#to minimize scores and estimate error for each species individually.
#
#August, 2014: Essentially version 2 of the script. Added the -f, -t, and -v options. Cleaned up
#the code and made some nice looking screen output. Importantly, the script now performs a final
#run of CAFE with the estimated error model AND generates a report for that run. This along with
#setting -f to 1 means that the entire error estimation process has been streamlined to just one
#run of caferror!
#############################################################################

import sys
import argparse
import os
import random
import datetime
import time
import re

#############################################################################
#Function definitions.
def IO_fileParse():
#This function handles the command line options.

	parser = argparse.ArgumentParser();

	parser.add_argument("-i", dest="input_file", help="A CAFE shell script with the full CAFE path in the shebang line, the load, and tree commands. These lines will be read and incorporated into the caferror shell script");
	parser.add_argument("-e", dest="user_err_start", help="The starting point for the grid search. Should be between 0 and 1. Default: 0.4", type=float, default=0.4);
	parser.add_argument("-d", dest="user_tmp_dir", help="A directory in which all caferror files will be stored. If none is specified, it will default to caferror_X, with X being some integer one higher than the last directory.", default="");
	parser.add_argument("-f", dest="first_run", help = "Boolean option to perform a pre-error model run (1) or not (0). Default: 0", type=int, default=1);
	parser.add_argument("-c", dest="curve_option", help="Boolean option. caferror can either perform the grid search (0) or search a pre-specified space (1). Default: 0", type=int, default=0);
	parser.add_argument("-t", dest="error_tries", help="A list of error values to search over. Note: -c MUST be set to 1 to use these values. Enter as a comma delimited string, ie -t 0.1,0.2,0.3", default="")
	parser.add_argument("-l", dest="user_log_file", help="Specify the name for caferror's log file here. Default: caferrorLog.txt", default="caferrorLog.txt");
	parser.add_argument("-o", dest="output_file", help="Output file which stores only the error model and score for each run. Default: caferror_default_output.txt", default="caferror_default_output.txt");
	parser.add_argument("-s", dest="ind_min", help="Boolean option to specify whether to perform only the global error search (0) or continue with individual species minimizations (1). Default: 0", type=int, default=0);
	parser.add_argument("-v", dest="verbose", help="Boolean option to have detailed information for each CAFE run printed to the screen (1) or not (0). Default: 1", type=int, default=1);
	parser.add_argument("-m", dest="run_mode", help=argparse.SUPPRESS, type=int, default=0);

	args = parser.parse_args();

	if args.input_file == None:
		parser.print_help();
		sys.exit();

	if args.first_run not in [0,1]:
		print(" ------------------------------------------------");
		print("|**Error 1: -f must take values of either 0 or 1 |");
		print(" ------------------------------------------------");
		parser.print_help();
		sys.exit();

	if args.user_err_start > 1 or args.user_err_start < 0:
		print(" -----------------------------------------------");
		print("|**Error 2: -e must take values between 0 and 1 |");
		print(" -----------------------------------------------");
		parser.print_help();
		sys.exit();

	if args.curve_option not in [0,1]:
		print(" ------------------------------------------------");
		print("|**Error 3: -c must take values of either 0 or 1 |");
		print(" ------------------------------------------------");
		parser.print_help();
		sys.exit();

	if args.ind_min not in [0,1]:
		print(" ------------------------------------------------");
		print("|**Error 4: -s must take values of either 0 or 1 |");
		print(" ------------------------------------------------");
		parser.print_help();
		sys.exit();

	if args.verbose not in [0,1]:
		print(" ------------------------------------------------");
		print("|**Error 5: -v must take values of either 0 or 1 |");
		print(" ------------------------------------------------");
		parser.print_help();
		sys.exit();

	if args.user_tmp_dir != "" and args.user_tmp_dir[len(args.user_tmp_dir) - 1] != "/":
		args.user_tmp_dir = args.user_tmp_dir + "/";

	if args.curve_option == 1 and args.error_tries == "":
		args.error_tries = [0.0, 0.001, 0.025, 0.05, 0.075, 0.1, 0.125, 0.15, 0.175, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95];
	elif args.curve_option == 1 and args.error_tries != "":
		args.error_tries = args.error_tries.split(",");
	
	### -m: Run mode for individual species minimzations (for debugging purposes only... not supported).
	###		0 - Default: No shuffle, background constant.
	###		1 - No shuffle, background updated.
	###		2 - Shuffle, background constant.
	###		3 - Shuffle, background updated.

	return args.input_file, args.output_file, args.user_err_start, args.user_tmp_dir, args.curve_option, args.error_tries, args.user_log_file, args.ind_min, args.run_mode, args.verbose, args.first_run;

#####################
def niceOut(infile,path,fam,t,lam,c,inite,ind,tries,tdir,elog,out,frun,vb):
#This function makes some nice output for the screen and log file.
	cLog = open(elog, "w");

	outline = "=======================================================================";
	redundantOut(outline,cLog);
	outline = "Using CAFE shell file:\t\t" + infile;
	redundantOut(outline,cLog);

	outline = "--------------------------";
	redundantOut(outline,cLog);
	outline = "\t\t\tINPUT INFO";
	print(outline);
	cLog.write(outline);
	cLog.write("\n");
	outline = "CAFE path set as:\t\t" + path;
	print(outline);
	outline = "CAFE path set as:\t\t\t" + path;
	cLog.write(outline);
	cLog.write("\n");
	outline = "Using gene family file:\t\t" + fam;
	redundantOut(outline,cLog);
	outline = "Using tree command:\t\t" + t;
	print(outline);
	outline = "Using tree command:\t\t\t" + t;
	cLog.write(outline);
	cLog.write("\n");
	outline = "Using lambda command:\t\t" + lam;
	redundantOut(outline,cLog);
	
	outline = "--------------------------";
	redundantOut(outline,cLog);
	outline = "\t\t\tOPTIONS INFO";
	redundantOut(outline,cLog);
	if c == 0:
		outline = "-c 0:\t\t\t\tPerforming global grid search.";
		print(outline);
		outline = "-c 0:\t\t\t\t\t\tPerforming global grid search.\n";
		cLog.write(outline);
		outline = "-e " + str(inite) + ":\t\t\t\t" + "Starting error estimate of " + str(inite) + ".";
		print(outline);
		outline = "-e " + str(inite) + ":\t\t\t\t\t\t" + "Starting error estimate of " + str(inite) + ".\n";
		cLog.write(outline);
	elif c == 1:
		outline = "-c 1:\t\t\t\tPerforming pre-set grid search";
		print(outline);
		outline = "-c 1:\t\t\t\t\t\tPerforming pre-set grid search\n";
		cLog.write(outline);
		outline = "Searching values (-t):\t\t" + str(tries);
		print(outline);
		outline = "Searching values (-t):\t\t\t\t" + str(tries);
		cLog.write(outline);
		outline = "";
		for x in xrange(len(tries)):
			if x != len(tries)-1:
				outline = outline + str(tries[x]) + ", ";
			else:
				outline = outline + str(tries[x]);
		cLog.write("\t\t");
		cLog.write(outline);
		cLog.write("\n");
	if frun == 0:
		outline = "-f 0:\t\t\t\tNo initial run without an errormodel (pre).";
		print(outline);
		outline = "-f 0:\t\t\t\t\t\tNo initial run without an errormodel (pre).\n";
		cLog.write(outline);
	elif frun == 1:
		outline = "-f 1:\t\t\t\tPerforming initial run without an errormodel (pre).";
		print(outline);
		outline = "-f 1:\t\t\t\t\t\tPerforming initial run without an errormodel (pre).\n";
		cLog.write(outline);
	if vb == 0:
		outline = "-v 0:\t\t\t\tNo CAFE output will be printed to the screen.";
		print(outline);
		outline = "-v 0:\t\t\t\t\t\tNo CAFE output will be printed to the screen.\n";
		cLog.write(outline);
	elif vb == 1:
		outline = "-v 1:\t\t\t\tPrinting all CAFE output to the screen.";
		print(outline);
		outline = "-v 1:\t\t\t\t\t\tPrinting all CAFE output to the screen.\n";
		cLog.write(outline);
	if ind == 0:
		outline = "-s 0:\t\t\t\tThis run WILL NOT do individual species minimizations.";
		print(outline);
		outline = "-s 0:\t\t\t\t\t\tThis run WILL NOT do individual species minimizations.\n";
		cLog.write(outline);
	elif ind == 1:
		outline = "-s 1:\t\t\t\tThis run WILL do individual species minimizations.";
		print(outline);
		outline = "-s 1:\t\t\t\t\t\tThis run WILL do individual species minimizations.\n";
		cLog.write(outline);

	outline = "--------------------------";
	redundantOut(outline,cLog);
	outline = "\t\t\tOUTPUT INFO";
	redundantOut(outline,cLog);
	outline = "Putting all files in:\t\t" + tdir;
	redundantOut(outline,cLog);
	outline = "Log file for all runs:\t\t" + elog;
	redundantOut(outline,cLog);
	outline = "Output file:\t\t\t" + outFilename;
	print(outline);
	outline = "Output file:\t\t\t\t\t" + outFilename + "\n";
	cLog.write(outline);
	outline = "=======================================================================";
	redundantOut(outline,cLog);

#####################
def redundantOut(oline, clogstream):
	print oline;
	clogstream.write(oline);
	clogstream.write("\n");

#####################
def getAllSpec(tmpTree):
#This function reads the Newick tree specified by the user in the input file for the Cafe runs and extracts a list of species which is
#necessary to write the caferror.sh script (Specifically the lines which indicate the species to which the error model is applied).

	if tmpTree.find(' ') != -1:
		tmpTree = tmpTree.replace(' ', '');
		tmpTree = tmpTree[4:];
		tmpTree = 'tree ' + tmpTree;

	if tmpTree.find(';') != -1:
		tmpTree = tmpTree.replace(';', '');

	tmpTree = tmpTree[5:];
	tmpTree = tmpTree.replace(',', ' ');
	tmpTree = tmpTree.replace(':', ' ');
	tmpTree = tmpTree.replace('(', '');
	tmpTree = tmpTree.replace(')', '');
	tmpTree = tmpTree.split(' ');

	x = len(tmpTree) - 1;

	while x >= 0:
		if tmpTree[x][0].isdigit():
			tmpTree.pop(x);
	
		x = x - 1;

	return tmpTree;

#####################
def cafeRun(families, newick, lamb, path, tempDir, cafLog, specDict, spectomin, logrm, fflag, nr, vb):
#This is the function which creates an error file based on the current error (by calling maxfamsize), writes the appropriate caferror.sh script
#and executes the current caferror.sh script.

	outline = "--------------------------";
	redundantOut(outline,cLog);
	outline = "CAFE run number " + str(nr);
	if spectomin[0] == "final":
		outline = outline + "(FINAL RUN)";
	redundantOut(outline,cLog);

	if spectomin[0] == "all":
		outline = str(specDict[errSpec[0]]) + " Error Model Run for all";
		redundantOut(outline,cLog);
		logFile = tempDir + "cafe_" + str(specDict[errSpec[0]]) + "_all_log.txt";
	elif spectomin[0] == "none":
		outline = "Pre run (no error models)";
		redundantOut(outline,cLog);
		logFile = tempDir + "cafe_pre_log.txt";
	elif spectomin[0] == "final":
		outline = "With minimized error models";
		redundantOut(outline,cLog);
		logFile = tempDir + "cafe_final_log.txt";
		repFile = tempDir + "cafe_final_report";
	else:
		outline = str(specDict[spectomin[0]]) + " Error Model Run for " +  spectomin[0];
		redundantOut(outline,cLog);
		logFile = tempDir + "cafe_" + str(specDict[spectomin[0]]) + "_" + spectomin[0] + "_log.txt";
	cLog.write("-----\n")
	#The above logic statements name the log file for the current CAFE run. The format for the log files is: cafe_[errormodel]_[species]_log.txt.
	#Special cases for species include 'all' in which the current errormodel is applied to all during global error estimation species and 'final' 
	#which is the last CAFE run with the minimized errormodels applied to each individual species.

	if logrm == 1:
		os.system("rm " + logFile);
	#In the event that CAFE initializes incorrectly, it must be re-run with the same parameters. When that happens, the old log file is removed here
	#and will be replaced with one of the same name. The contents of the logfile are saved to the 'badRuns.txt' file in the getScore function.

	if len(spectomin) == 1:
	#For global error prediction (spectomin = ['all']), the final CAFE run (spectomin = ['final']), and individual species minimizations (spectomin = [the
	#current species] the spectomin list will contain just one element.
		if spectomin[0] == "none":
			outline = "## No error models generated\n";
			cLog.write(outline);			
		else:
			for key in specDict:
				ErrString = "cafe_errormodel_" + str(specDict[key]) + ".txt";
				ErrFile = tempDir + ErrString

				genErrFile = errFileCheck(key, ErrString, specDict, tempDir)
				#This line calls the errFileCheck function to see if the error model file for this error distribution and this species is already created.

				if genErrFile == 1:
					maxfamsize(families, specDict[key], ErrFile);
					#This line calls the maxfamsize function below to create the error file.

	else:
	#When -g is specified in the command line, spectomin will contain more than one element. [NOTE: -g not yet developed]
		for key in spectomin:

			ErrString = "cafe_errormodel_" + str(specDict[key]) + ".txt";
			ErrFile = tempDir + ErrString
		
			genErrFile = errFileCheck(key, ErrString, specDict, tempDir)
			#This line calls the errFileCheck function to see if the error model file for this error distribution and this species is already created.

			if genErrFile == 1:
				maxfamsize(families, specDict[key], ErrFile);
				#This line calls the maxfamsize function below to create the error file.
	#The above blocks create the error file for the current run (if necessary) by calling the errFileCheck and maxfamsize functions.

	#####
	cLog.write("-----\n");
	print("Rewriting CAFE shell script...");
	cLog.write("Rewriting CAFE shell script...\n");

	##########
	#These lines write the caferror.sh script.
	shellfile = "caferror.sh";

#	shellfileList = os.listdir(path);
#	shellcount = 1;
#	while shellfile in shellfileList:
#		shellfile = "caferror" + str(shellcount) + ".sh";
#		shellcount = shellcount + 1;
#	cLog.write("\nshellfile: " + shellfile + "\n");
#	cLog.write("logfile: " + logFile + "\n");
#	#Some lines for debugging. Uncomment as desired.

	cafeFile = open(shellfile, "w");

	cafeFile.write(path);
	cafeFile.write("\n");
	cafeFile.write(newick);
	cafeFile.write("\n");

	if fflag == 0:
		loadLine = "load -i " + families + " -t 10 -l " + logFile + "\n"
	elif fflag == 1:
		loadLine = "load -i " + families + " -t 10 -l " + logFile + " -filter\n"

	cafeFile.write(loadLine);

	if spectomin[0] != "none":
		if len(spectomin) == 1:
			for key in specDict:
				ErrString = "cafe_errormodel_" + str(specDict[key]) + ".txt";
				ErrFile = tempDir + ErrString;
				errLine = "errormodel -model " + ErrFile + " -sp " + key + "\n";
				cafeFile.write(errLine);	
	
		else:
			for key in spectomin:
				ErrString = "cafe_errormodel_" + str(specDict[key]) + ".txt";
				ErrFile = tempDir + ErrString;
				errLine = "errormodel -model " + ErrFile + " -sp " + key + "\n";
				cafeFile.write(errLine);	

	cafeFile.write(lamb);
	cafeFile.write("\n");
	if spectomin[0] == "final":
		repline = "report " + repFile;
		cafeFile.write(repline);

	cafeFile.close();
	#End caferror.sh writing.
	##########
	os.system("chmod +x " + shellfile);
	if vb == 0:
		outline = "Running CAFE [silently] with error models listed above...";
		redundantOut(outline,cLog);
		os.system("./" + shellfile + " >> " + tempDir + "cafe.out 2>&1");
	else:
		outline = "Running CAFE with error models listed above...";
		redundantOut(outline,cLog);
		os.system("./" + shellfile);
	nr = nr + 1;
	#These lines give the caferror.sh script permissions and execute it to run cafe given the current parameters.

	print("CAFE run complete! Retrieving Score..........");
	#This is printed here. Score retrieval is actually handled by getScore (directly below), which is always called immediately after 
	#this function in the main block.

	return nr;

#####################
def errFileCheck(errspec_check, errstring_check, specdict_check, thedir):
#This function checks if, for a given species and error model, the error model file has already been created. It returns 1 the error model file
#has not yet been created and 0 if it already has.

	fileList = os.listdir(thedir);

	if errstring_check in fileList:
		cLog.write("## ");
		cLog.write(str(specdict_check[errspec_check]));
		cLog.write(" Error File Already Created for ")
		cLog.write(errspec_check)
		cLog.write("\n")

		return 0;

	else:
		cLog.write("## Generating ");
		cLog.write(str(specdict_check[errspec_check]));
		cLog.write(" Error File for ")
		cLog.write(errspec_check)
		cLog.write("\n")
	
		return 1;

#####################
def getScore(error, spectomin, tempDir, wout):
#This function retrieves the score calculated by CAFE from a given log file, specified by the amount of error and species used in that run.
#This function now also checks if the previous CAFE run was initialized properly. If so, the program will continue and if not it will send
#a signal back to the call telling it to re-run CAFE with the same parameters.

	init = "good";
	lcount = 0;
	linfcount = 0;

	if spectomin[0] == "all":
		scoreFile = tempDir + "cafe_" + str(error) + "_all_log.txt";
	elif spectomin[0] == "final":
		scoreFile = tempDir + "cafe_final_log.txt";
	elif spectomin[0] == "none":
		scoreFile = tempDir + "cafe_pre_log.txt";
	else:
		scoreFile = tempDir + "cafe_" + str(error) + "_" + spectomin[0] + "_log.txt";

	sFile = open(scoreFile, "r");
	sLines = sFile.readlines();
	sFile.close();

	initFile = open(tempDir + "InitFile.txt", "a");
	initFile.write(scoreFile);
	initFile.write("\n")

	for s in xrange(len(sLines)):
		if sLines[s][:7] == "Poisson":
			initFile.write(sLines[s]);
		if sLines[s][:7] == "Poisson" and sLines[s].find("inf") != -1:
			init = "bad";
		if sLines[s][:7] == ".Lambda":
			lcount = lcount + 1;
		if sLines[s][:7] == ".Lambda" and sLines[s].find("inf") != -1:
			linfcount = linfcount + 1;

		if sLines[s].find("Lambda Search Result:") != -1:
			lamval = sLines[s+1][9:sLines[s+1].index("&") - 1];
			score = sLines[s+1][sLines[s+1].index("Score: ") + 7:].replace("\n","");
			#score = score.replace('\n', '');

	if lcount == linfcount:
		init = "bad";

	initFile.write(str(score));
	initFile.write("\n");
	initFile.write(init);
	initFile.write("\n\n");
	initFile.close();

	outline = "Score with above error models:\t" + str(score);
	redundantOut(outline,cLog);
	outline = "Lambda with above error models:\t" + str(lamval);
	redundantOut(outline,cLog);

	if init == "bad":
		outline = "++WARNING: CAFE failed to converge or initialize. This run will be re-done. Check badRuns.txt for more info.\n";
		redundantOut(outline,cLog);
		bFile = open(tempDir + "badRuns.txt", "a");
		bFile.write("\n************\n\n")
		bFile.write(scoreFile);
		bFile.write("\n");
		for sline in sLines:
			bFile.write(sline);
		bFile.close();

	if init == "good":
		if wout == 1:
			outFile = open(outFilename, "a");
			line = str(error) + "\t" + score + "\n"
			outFile.write(line);
			outFile.close();

	return float(score), float(lamval), init;

#####################
def maxfamsize(inFilename, totError, errFilename):
#This function creates an error file in the proper format for Cafe by reading the input gene family file and
#finding the gene family with the largest number of genes.

	negAsym = 0.5;
	posAsym = 1 - negAsym;
	#I used this to run some simulations with asymmetric error distributions. Right now it is set to split the error evenly
	#between +1 and -1, but go ahead and change negAsym if needed.

	inFile = open(inFilename, "r");
	lines = inFile.readlines();
	inFile.close();

	maxfs = 0;

	for i in range(len(lines)):
		if i == 0:
			continue;		
		cline = lines[i].replace("\n","").split("\t");	
		k = 2;
	
		while k <= len(cline) - 1:
			if int(cline[k]) > maxfs:
				maxfs = int(cline[k]);			
			k = k + 1;

	erroutFile = open(errFilename, "w");
	erroutFile.write("maxcnt:");
	erroutFile.write(str(maxfs));
	erroutFile.write("\n");
	erroutFile.write("cntdiff -1 0 1\n");

	j = 0;

	while j <= maxfs:
		if j == 0:
			pline = str(j) + " 0.00 " + str((totError / 2) + (1 - totError)) + " " + str(totError / 2);	
			erroutFile.write(pline);
		else:
			pline = str(j) + " " + str(totError * negAsym) + " " + str(1 - totError) + " " + str(totError * posAsym);		
			erroutFile.write(pline);

		if j != maxfs:
			erroutFile.write("\n");
		j = j + 1;
	
	erroutFile.close();

#####################

#############################################################################
#Main Block
#############################################################################

inFilename, outFilename, initError, tmpDir, wholeCurveOpt, errTries, caferrorLog, indSpecMin, Mode, vOpt, firstRun = IO_fileParse();
#The first step is to retrieve the proper values for options and filenames based on the user's command line specifications.
#errTries = [0.0, 0.001, 0.025, 0.05, 0.075, 0.1, 0.125, 0.15, 0.175, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95];

initCheck = "";
spec_to_min = [];
rmlog = 0;
FilterFlag = 0;

inFile = open(inFilename, "r");
inLines = inFile.readlines();
inFile.close();

for x in range(len(inLines)):
	if inLines[x][:2] == "#!":
		CafePath = inLines[x].replace("\n","");

	if inLines[x][:4] == "load":
		FamFile = inLines[x][(inLines[x].index("-i") + 3):];
		FamFile = FamFile[:FamFile.index(" ")];

		if inLines[x].find("-filter") != -1:
			FilterFlag = 1;

	if inLines[x][:4] == "tree":
		Tree = inLines[x].replace('\n', '');

	if inLines[x][:6] == "lambda":
		LamStruct = inLines[x].replace('\n', '');
#Obtains input variables from an existing CAFE script specified with -i.

errSpec = getAllSpec(Tree);
#print errSpec;
#sys.exit()

mainSpecDict = {};
for eachspec in errSpec:
	mainSpecDict[eachspec] = initError;
spec_to_min.append("all");
#Some initializations of important variables above. 
#errSpec gets a list of all species in the input tree. 
#Throughout the program, mainSpecDict will keep track of the species and their corresponding minimized errors. It follows the [key]:[value] format
#of [species]:[minimized error model].
#spec_to_min is what will be passed as the species to minimize for the global error estimation algorithm. Usually this will be passed simply 
#as ['all'].

#########
#This block creates a new default directory for each run of caferror.py
if tmpDir == "":
	prevList = os.listdir(os.getcwd());

	isDirMade = 0;
	x = 1

	while isDirMade == 0:
		dirName = "caferror_" + str(x);
		if dirName not in prevList:		
			tmpDir = os.getcwd() + "/" + dirName + 	"/";
			isDirMade = 1;
		else:
			x = x + 1;

	if tmpDir.find("//") != -1:
		tmpDir = tmpDir.replace("//", "/");

	os.system("mkdir " + tmpDir);
	#If the user didn't define a temp directory with -d, the default value is set here. This also handles a possible error with the double slashes.

else:
	os.system("mkdir " + tmpDir);

#End default dir block
##########

caferrorLog = tmpDir + caferrorLog;
if caferrorLog.find("//") != -1:
	caferrorLog = caferrorLog.replace("//", "/");
#If the user didn't define a log file with -l, the default value is set here. This also handles a possible error with the double slashes.

outFilename = tmpDir + outFilename;
outFile = open(outFilename, "w");
outFile.write("ErrorModel\tScore\n");
outFile.close();
#This primes the output file with the column headers.

niceOut(inFilename,CafePath,FamFile,Tree,LamStruct,wholeCurveOpt,initError,indSpecMin,errTries,tmpDir,caferrorLog,outFilename,firstRun,vOpt);

startsec = time.time();
start = datetime.datetime.now();

cLog = open(caferrorLog, "a");

outline = "Caferror started at:\t" + str(start);
redundantOut(outline,cLog);

print("Beginning Global Error Prediction...");
cLog.write("Beginning Global Error Prediction...\n");

numruns = 1;

if firstRun == 1:
	spec_to_min[0] = "none";
	while initCheck == "bad" or initCheck == "":
		numruns = cafeRun(FamFile, Tree, LamStruct, CafePath, tmpDir, caferrorLog, mainSpecDict, spec_to_min, rmlog, FilterFlag, numruns, vOpt);
		lastScore, lastLam, initCheck = getScore(initError, spec_to_min, tmpDir, 1);
		if initCheck == "bad":
			rmlog = 1;
	initCheck = "";
	rmlog = 0;
	preScore = lastScore;
	preLam = lastLam;
	spec_to_min[0] = "all";
#The pre-error model run if -f 1.

####################################
#Global error estimation begins here for -c 0 (default)

if wholeCurveOpt == 0:
#If -c is set to 0, which is the default value, caferror enters this block to predict error in an iterative fashion that uses information from the 
#previous run until a minimum score is achieved. Caferror starts at the error value specified by -e (0.4 by default).
	errList = [];
	errList.append(initError);

	errMin = initError;
	#Initializations of some variables. The 'Holder' variables are used to keep the place of parameters which are un-needed for any one cafeRun call.

	while initCheck == "bad" or initCheck == "":
		numruns = cafeRun(FamFile, Tree, LamStruct, CafePath, tmpDir, caferrorLog, mainSpecDict, spec_to_min, rmlog, FilterFlag, numruns, vOpt);
		lastScore, lastLam, initCheck = getScore(initError, spec_to_min, tmpDir, 1);
		if initCheck == "bad":
			rmlog = 1;
	initCheck = "";
	rmlog = 0;
	#The initial Cafe run using the error valued specified by -e (0.4 by default).

	minScore = lastScore;
	errMin = initError;
	lastError = initError;

	nextError = 0.0;
	posLimit = 1.0;
	posLimScore = lastScore;
	negLimit = 0.0;
	negLimScore = 0.0; 

	currentError = initError / 2;
	errList.append(currentError);

	for eachspec in mainSpecDict:
		mainSpecDict[eachspec] = currentError;

	tally = 0;
	keepGoing = 1;

	while keepGoing == 1:
	#This will keep guessing error models until one of two termination scenarios are met. These scenarios are checked for at the
	#end of each CAFE run below.

		tally = tally + 1;

		errList.append(currentError);
		while initCheck == "bad" or initCheck == "":
			numruns = cafeRun(FamFile, Tree, LamStruct, CafePath, tmpDir, caferrorLog, mainSpecDict, spec_to_min, rmlog, FilterFlag, numruns, vOpt);
			currentScore, currentLam, initCheck = getScore(currentError, spec_to_min, tmpDir, 1);
			if initCheck == "bad":
				rmlog = 1;
		initCheck = "";
		rmlog = 0;

#		outFile = open(outFilename, "a");
#		line = str(currentError) + "\t" + str(currentScore) + "\n"
#		outFile.write(line);
#		outFile.close();
#		print("++++++++++";
#		print errMin, minScore;
#		print negLimit, posLimit;
#		print("++++++++++";
#		cLog.write("errMin and minScore before logic: ");
#		cLog.write(str(errMin));
#		cLog.write(", ");
#		cLog.write(str(minScore));
#		cLog.write("\n");
#		cLog.write("negLimit and posLimit before logic: ");
#		cLog.write(str(negLimit));
#		cLog.write(", ");
#		cLog.write(str(posLimit));
#		cLog.write("\n");
		#Some print statements to track values for debugging. Uncomment as desired.

		##################
		#The main logic statements of the code are below. These take the score and error values of the most recent CAFE run
		#and evaluate them against some information obtained from previous runs, eventually arriving at a point 
		#where the termination scenarios are met with a minimum score value.
		if currentScore < minScore:
			if currentError < lastError:
				nextError = currentError / 2;

				posLimit = lastError * 2;
				posLimScore = lastScore;

			elif currentError > lastError:
				nextError = currentError * 2;

				negLimit = lastError / 2;
				megLimScore = lastScore;

			minScore = currentScore;	
			minLam = currentLam;
			errMin = currentError;

		elif currentScore > minScore:
			if currentError < errMin:
				nextError = errMin * 2

				negLimit = currentError;
				negLimScore = currentScore;

			elif currentError > errMin:
				nextError = errMin / 2;

				posLimit = currentError;
				posLimScore = currentScore;

#		print("----------";
#		print errMin, minScore;
#		print negLimit, posLimit;
#		print("----------";
#		cLog.write("errMin and minScore after logic: ");
#		cLog.write(str(errMin));
#		cLog.write(", ");
#		cLog.write(str(minScore));
#		cLog.write("\n");
#		cLog.write("negLimit and posLimit after logic: ");
#		cLog.write(str(negLimit));
#		cLog.write(", ");
#		cLog.write(str(posLimit));
#		cLog.write("\n");
		#Some print statements to track values for debugging. Uncomment as desired.
	
		if nextError > posLimit:
			nextError = ((currentError + posLimit) / 2);
		if nextError < negLimit:
			nextError = ((currentError + negLimit) / 2);
		if nextError in errList:
			nextError = ((currentError + nextError) / 2);

#		print("***************";
#		print currentError;
#		print len(str(currentError));
#		print("***************";
		#Some print statements to track values for debugging. Uncomment as desired.

		lastError = currentError;

		#####
		#These are the termination scenarios. If the error parameter reaches a certain level of precision (12 decimal places), or if
		#CAFE has been run 20 times, error estimation is terminated. This may be improved.
		if len(str(currentError)) >= 15:
			keepGoing = 0;
		elif tally > 20:
			keepGoing = 0;
		#End termination block.
		#####
		else:
			currentError = nextError;
			for eachspec in mainSpecDict:
				mainSpecDict[eachspec] = nextError;
			lastScore = currentScore;
		#End logic block.
		##################

#Global error estimation ends here for -c 0 (default)
####################################



####################################
#Global error estimation begins here for -c 1

elif wholeCurveOpt == 1:
#If -c is set to 1 by the user caferror enters this block to calculate scores for a pre-set set of values [0,1).
#This will be less precise than the method above, but will give the user an easy to visualize output when plotted.

	minScore = 1000000000.0;
	errMin = 1.0;
	#Initializations of some variables.


	for currentError in errTries:
	#This runs Cafe using all error models in the list 'errTries' defined above. Feel free to add or remove values as desired.

		for eachspec in mainSpecDict:
			mainSpecDict[eachspec] = currentError;
	
		while initCheck == "bad" or initCheck == "":
			numruns = cafeRun(FamFile, Tree, LamStruct, CafePath, tmpDir, caferrorLog, mainSpecDict, spec_to_min, rmlog, FilterFlag, numruns, vOpt);
			currentScore, currentLam, initCheck = getScore(currentError, spec_to_min, tmpDir, 1);
			if initCheck == "bad":
				rmlog = 1;
		initCheck = "";
		rmlog = 0;

		if float(currentScore) < minScore and minScore != float("infinity"):
			minScore = float(currentScore);
			minLam = currentLam;
			errMin = currentError;


#Global error estimation ends here for -c 1
####################################


####################################
#Individual species error estimation begins here if -s 1
#
#After the above algorithm has minimized error scores across all species (by applying the same error model to all species simultaneously), the user has
#the option to continue minimization for each individual species. This is done by iteratively adding and subtracting 10% of the global min error from each
#current species's error model until the score ceases to improve.

#errMin = 0.04765625;
#minScore = 50384.197476;
#minLam = 0.27626649636157;

finalSpecDict = {};

if Mode == 2 or Mode == 3:
	random.shuffle(errSpec);

if Mode == 1 or Mode == 3:
	for eachspec in mainSpecDict:
		mainSpecDict[eachspec] = errMin;
	overallMinScore = minScore;

for eachspec in mainSpecDict:
	finalSpecDict[eachspec] = 0.0

if indSpecMin == 1:
	outline = "***********************************************************************"
	redundantOut(outline,cLog);

	outline = "Global error prediction complete. Beginning error minimization for individual species.";
	redundantOut(outline,cLog);

	moreErrSpec = [];

	for curSpec in errSpec:
		outline = "--------------------------";
		redundantOut(outline,cLog);

		outline = "**Attempting to Minimize " + curSpec + " by adding more error";
		redundantOut(outline,cLog);

		if Mode == 0 or Mode == 2:
			specMinScore = minScore;
		if Mode == 1 or Mode == 3:
			specMinScore = overallMinScore;
		oldspecMin = minScore;

		zcheck = 0;
		minimized = 0;
		currentError = errMin + (0.1 * errMin);
		#Some initializations.

		if Mode == 1 or Mode == 3:
			specMinErr = errMin;

		if Mode == 0 or Mode == 2:
			for eachspec in mainSpecDict:
				mainSpecDict[eachspec] = errMin;

		spec_to_min[0] = curSpec;

		while minimized == 0:
		#For each species, 10% of the global min error will be iteratively added in this loop until the score ceases to decrease OR the error has reached 1.0. At which point minimized will be set
		#to 1 and the loop will exit.

			mainSpecDict[curSpec] = currentError;

			if mainSpecDict[curSpec] <= 1.0:
				while initCheck == "bad" or initCheck == "":
					numruns = cafeRun(FamFile, Tree, LamStruct, CafePath, tmpDir, caferrorLog, mainSpecDict, spec_to_min, rmlog, FilterFlag, numruns, vOpt);
					currentScore, currentLam, initCheck = getScore(currentError, spec_to_min, tmpDir, 0);
					if initCheck == "bad":
						rmlog = 1;
				initCheck = "";
				rmlog = 0;

			else:
				minimized = 1;
				continue;

			if float(currentScore) > float(specMinScore):
				check = "higher than";
				check2 = "will not be further minimized.";

				if finalSpecDict[curSpec] == 0.0:
					finalSpecDict[curSpec] = errMin;

				oldspecMin = specMinScore;
				minimized = 1;

			elif float(currentScore) == float(specMinScore):
				check = "the same as";
				check2 = "will not be further minimized.";
				oldspecMin = specMinScore;
				minimized = 1;

			elif float(currentScore) < float(specMinScore):
				check = "lower than";
				check2 = "will continue to be minimized...";

				if finalSpecDict[curSpec] == 0.0:
					finalSpecDict[curSpec] = errMin;

				oldspecMin = specMinScore;
				specMinScore = currentScore;

				if zcheck == 1:
					minimized = 1;

				finalSpecDict[curSpec] = currentError;

				specMinErr = currentError;

### 				if Mode == 1 or Mode == 3:
###					overallMinScore = currentScore;
###				Toggle this line to either keep the background constant (commented) or update it each time a species is minimized.

				currentError = currentError + (0.1 * errMin);

				if currentError >= 1:
					currentError = 1.0
					zcheck = 1;

				if curSpec not in moreErrSpec:
					moreErrSpec.append(curSpec);

		if Mode == 1 or Mode == 3:
			mainSpecDict[curSpec] = specMinErr;
			
	for curSpec in errSpec:
	#This block iteratively subtracts 10% of the global min error to each species' error model to attempt to find out which species contain LESS error than
	#the global min, and to get a general guess of what that error might be.
		if curSpec not in moreErrSpec:

			outline = "--------------------------";
			redundantOut(outline,cLog);
			outline = "**Attempting to Minimize " + curSpec + " by adding less error";
			redundantOut(outline,cLog);

			if Mode == 0 or Mode == 2:
				specMinScore = minScore;
			if Mode == 1 or Mode == 3:
				specMinScore = overallMinScore;
			oldspecMin = minScore;

			zcheck = 0;
			minimized = 0;
			currentError = errMin - (0.1 * errMin);

			if Mode == 1 or Mode == 3:
				specMinErr = errMin;

			if Mode == 0 or Mode == 2:
				for eachspec in mainSpecDict:
					mainSpecDict[eachspec] = errMin;

			spec_to_min[0] = curSpec;

			while minimized == 0:
			#For each species, 10% of the global min error will be iteratively subtracted in this loop until the score ceases to decrease OR the error has reached 0.0.
			#At which point minimized will be set to 1 and the loop will exit.
				mainSpecDict[curSpec] = currentError;
				
				if mainSpecDict[curSpec] >= 0.0:
					while initCheck == "bad" or initCheck == "":
						numruns = cafeRun(FamFile, Tree, LamStruct, CafePath, tmpDir, caferrorLog, mainSpecDict, spec_to_min, rmlog, FilterFlag, numruns, vOpt);
						currentScore, currentLam, initCheck = getScore(currentError, spec_to_min, tmpDir, 0);
						if initCheck == "bad":
							rmlog = 1;
					initCheck = "";
					rmlog = 0;
				else:
					minimized = 1;
					continue;
				
				if float(currentScore) > float(specMinScore):
					check = "higher than";
					check2 = "will not be further minimized.";

					if finalSpecDict[curSpec] == 0.0:
						finalSpecDict[curSpec] = errMin;

					oldspecMin = specMinScore;
					minimized = 1;

				elif float(currentScore) == float(specMinScore):
					check = "the same as";
					check2 = "will not be further minimized.";

					if finalSpecDict[curSpec] == 0.0:
						finalSpecDict[curSpec] = errMin;

					oldspecMin = specMinScore;
					minimized = 1;

				elif float(currentScore) < float(specMinScore):
					check = "lower than";
					check2 = "will continue to be minimized...";
					oldspecMin = specMinScore;
					specMinScore = currentScore;

					if zcheck == 1:
						minimized = 1;

					finalSpecDict[curSpec] = currentError;

## #					if Mode == 1 or Mode == 3:
					specMinErr = currentError;
###					Toggle this line to either keep the background constant (commented) or update it each time a species is minimized.

		
					currentError = currentError - (0.1 * errMin);
	
					if currentError <= 0:
						currentError = 0.0;
						zcheck = 1;
					
			if Mode == 1 or Mode == 3:
				mainSpecDict[curSpec] = specMinErr;


#Individual species error estimation ends here if -s 1
####################################

	#####
	#The final CAFE run which generates a report with the minimized error.
	cLog.write("\n***********************************************************************\n")

	spec_to_min[0] = "final";

	while initCheck == "bad" or initCheck == "":
		numrnuns = cafeRun(FamFile, Tree, LamStruct, CafePath, tmpDir, caferrorLog, finalSpecDict, spec_to_min, rmlog, FilterFlag, numruns, vOpt);
		finalScore, finalLam, initCheck = getScore("final", spec_to_min, tmpDir, 0);
		if initCheck == "bad":
			rmlog = 1;
	initCheck = "";
	rmlog = 0;
	#####

####################################

if indSpecMin == 1:
	outline = "=======================================================================";
	redundantOut(outline,cLog);

	outline = "Final error estimates by species:";
	redundantOut(outline,cLog);
	
	for key in finalSpecDict:
		outline = key + "\t" + str(finalSpecDict[key]);
		redundantOut(outline,cLog);

	outline = "Score with individual errors:\t" + str(finalScore);
	redundantOut(outline,cLog);

	outline = "Lambda with individual errors:\t" + str(finalLam);
	redundantOut(outline,cLog);

	if firstRun == 1:
		outline = "************************************";
		redundantOut(outline,cLog);
		outline = "Score with no errormodel:\t" + str(preScore);
		redundantOut(outline,cLog);
		outline = "Lambda with no errormodel:\t" + str(preLam);
		redundantOut(outline,cLog);

	outline = "************************************";
	redundantOut(outline,cLog);
	outline = "Global Error Estimation:\t" + str(errMin);
	redundantOut(outline,cLog);
	outline = "Score with global errormodel:\t" + str(minScore);
	redundantOut(outline,cLog);
	outline = "Lambda with global errormodel:\t" + str(minLam);
	redundantOut(outline,cLog);
	outline = "************************************";
	redundantOut(outline,cLog);	

else:
	spec_to_min[0] = "final";

	for eachspec in mainSpecDict:
		mainSpecDict[eachspec] = errMin;

	while initCheck == "bad" or initCheck == "":
		numrnuns = cafeRun(FamFile, Tree, LamStruct, CafePath, tmpDir, caferrorLog, mainSpecDict, spec_to_min, rmlog, FilterFlag, numruns, vOpt);
		finalScore, finalLam, initCheck = getScore("final", spec_to_min, tmpDir, 0);
		if initCheck == "bad":
			rmlog = 1;
	initCheck = "";
	rmlog = 0;

	####################################
	#Output block if -s 0

	outline = "=======================================================================";
	redundantOut(outline,cLog);
	if firstRun == 1:
		outline = "************************************";
		redundantOut(outline,cLog);
		outline = "Score with no errormodel:\t" + str(preScore);
		redundantOut(outline,cLog);
		outline = "Lambda with no errormodel:\t" + str(preLam);
		redundantOut(outline,cLog);

	outline = "************************************";
	redundantOut(outline,cLog);
	outline = "Global Error Estimation:\t" + str(errMin);
	redundantOut(outline,cLog);
	outline = "Score with global errormodel:\t" + str(minScore);
	redundantOut(outline,cLog);
	outline = "Lambda with global errormodel:\t" + str(minLam);
	redundantOut(outline,cLog);
	outline = "************************************";
	redundantOut(outline,cLog);	


outline = "=======================================================================";
redundantOut(outline,cLog);

endsec = time.time();
runtime = (float(endsec) - float(startsec)) / float(60.0);
end = datetime.datetime.now();

outline = "Caferror finished at:\t" + str(end);
redundantOut(outline,cLog);
outline = "Runtime = " + str(runtime) + " minutes";
redundantOut(outline,cLog);
cLog.close();
#############################################################################
#End Main Block
#############################################################################





