#!/usr/bin/python
########################################################################################
#This script is part of the gene family clustering pipeline. It calculates some 
#summary statistics for an MCL dump file which contains the clusters from and MCL
#run. The syntax of the labels is as follows:
#
#"ENSAME ENSBTA ENSCAF ENSECA ENSFCA ENSLAF ENSMPU ENSP00 ENSSSC"
#"AALB AARA AATE ACHR ACUA ADAC ADIR AEPI AFAF AFUN AGAP AMAM AMEC AMEM AMIN AQUA ASIS ASTE"
#
#The entire list must be surrounded by quotes and labels are separated by spaces. The
#labels must be unique strings for each species in their gene identifiers.
#
#Sample usage: python mcl_analysis_2.py -i [MCL dump. file] -l ["species labels"]
#
#Results will be printed to the screen, but can also be piped to a file by using the ">"
#in the terminal:
#
#python mcl_analysis_2.py -i [MCL dump. file] -l ["species labels"] > [output_file]
#
#Gregg Thomas, August 2013
########################################################################################

import sys
import argparse

############################################
#Function definitions.

def IO_fileParse():
#This function handles the command line options.

	parser = argparse.ArgumentParser();

	parser.add_argument("-i", dest="input_file", help="MCL dump file.");
	parser.add_argument("-l", dest="spec_labels", help="Each species should have a unique identifier in their gene IDs. Enter them here as a space delimited list in quotes (ie \"ENSFCA,ENSCAF\")");

	args = parser.parse_args();

	if args.input_file == None:
		parser.print_help();
		sys.exit();

	if args.spec_labels == None:
		spec_labels = "";
	else:
		spec_labels = args.spec_labels;

	return args.input_file, spec_labels;


############################################
#Main Block
############################################

infilename, labels = IO_fileParse();
#Retrieves command line options.

print "=======================================================================";
print "Analyzing file:\t\t" + infilename;
if labels != "":
	print "Using species labels:\t" + labels;
else:
	print "Not using species labels to count exclusive orthologs.";


labels = labels.split(" ");
total_seqs = 0;
num_clusters = 0;
smallest = 1000;
largest = 0;
num_single = 0;
countDict = {};
num_oto = 0;
#All the variable declarations.

for line in open(infilename, "r"):
#Each line consists of one cluster, so this loop reads through all clusters.
	num_clusters = num_clusters + 1;
	
	tmpline = line.replace("\n", "");
	tmpline = tmpline.split("\t");
	cur_size = len(tmpline);
		
	total_seqs = total_seqs + cur_size;

	if cur_size in countDict:
		countDict[cur_size] = countDict[cur_size] + 1;
	else:
		countDict[cur_size] = 1;

	if cur_size == 1:
		num_single = num_single + 1;

	if cur_size > largest:
		largest = cur_size;
	if cur_size < smallest:
		smallest = cur_size;

	##If labels are defined, one-to-one clusters are identified here.
	if labels != "" and cur_size == len(labels):	
		labels_count = {}
		for label in labels:
			labels_count[label] = 0;

		for label in labels_count:
			for seq in tmpline:
				if seq.find(label) != -1:
					labels_count[label] = labels_count[label] + 1;

		oto_flag = 1;
		for label in labels_count:
			if labels_count[label] != 1:
				oto_flag = 0;

		if oto_flag == 1:
			num_oto = num_oto + 1;
	##


mean = float(total_seqs) / float(num_clusters);
per_single = round(float(num_single) / float(num_clusters) * 100, 1);
#Some calculations.

print "--------------------------------------------------";
print total_seqs, "seqs in", num_clusters, "clusters";
print "Cluster size range =", smallest, "-", largest;
print "Mean =", mean;
print "Single-seq clusters =", num_single, "(" + str(per_single)  + "%)"
print "--------------------------------------------------";
print "Number of clusters by size (1-10 | 11-20+)";

outline = "";
for x in xrange(1,21):
	if x in countDict:
		if x == 10:
			outline = outline + str(countDict[x]) + " | ";
		elif x == 20:
			outline = outline + str(countDict[x]);
		else:
			outline = outline + str(countDict[x]) + ",";
	else:
		if x == 10:
			outline = outline + "0 | ";
		if x == 20:
			outline = outline + "0";
		else:
			outline = outline + "0,";

print outline;


if labels != "":
	print "--------------------------------------------------";
	print "Number of exclusive ortholog clusters:", num_oto;
	print "=======================================================================";
else:
	print "=======================================================================";





