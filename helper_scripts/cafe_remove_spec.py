#!/usr/bin/python
############################################
#CAFE helper script to remove one or more 
#species from the input file.
#
#Sample usage: python cafe_remove_spec.py -i [cafe_input_file] -l "spec1 spec2" -o [output_filename]
############################################

import sys, os, argparse
sys.path.append(sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../corelib/"))
import core

############################################
#Function Definitions
############################################
def IO_fileParse():
#This function handles the command line options.

	parser = argparse.ArgumentParser();

	parser.add_argument("-i", dest="input_file", help="CAFE input file from which you wish to remove a species.");
	parser.add_argument("-l", dest="spec_labels", help="A space delimited list of species you wish to remove. The entire list should be contained in quotes.");
	parser.add_argument("-o", dest="output_file", help="Desired output file name.");

	args = parser.parse_args();

	if args.input_file == None or args.spec_labels == None or args.output_file == None:
		parser.print_help();
		sys.exit();

	return args.input_file, args.spec_labels, args.output_file;

############################################
#Main Block
############################################

infilename, speclist, outfilename = IO_fileParse();
speclist = speclist.split(" ");

print "====================================";
print "Input File:\t\t" + infilename;
print "Removing columns:\t" + str(speclist);

rotator = 0;

i = 0;
colrem = [];

for line in open(infilename):

	rotator = core.loadingRotator(i,rotator,10)

	if i == 0:
		for each in speclist:
			if each not in line:
				print " ---------------------------------------------------------------------------";
				print "|**Error 1: Species label(s) not found in input file. Re-check your labels! |";
				print " ---------------------------------------------------------------------------";
				sys.exit();

		tmpline = line.replace("\n", "");
		tmpline = tmpline.split("\t");

		newline = [];
		numspec = len(tmpline) - 2;
		
		for x in xrange(len(tmpline)):
			if tmpline[x] in speclist:
				colrem.append(x);
				numspec = numspec - 1;
			else:
				newline.append(tmpline[x]);

		newline = "\t".join(newline);
		newline = newline + "\n";

		outFile = open(outfilename, "w");
		outFile.write(newline);
		outFile.close();		

		i = i + 1;
		continue;

	else:
		tmpline = line.replace("\n", "");
		tmpline = tmpline.split("\t");

		newline = [];

		for x in xrange(len(tmpline)):
			if x not in colrem:
				newline.append(tmpline[x]);

		numpos = numspec;

		for each in newline:
			if each == '0':
				numpos = numpos - 1;

		if numpos > 1:
			newline = "\t".join(newline);
			newline = newline + "\n";

			outFile = open(outfilename, "a");
			outFile.write(newline);
			outFile.close();	
	i = i + 1;

sys.stderr.write('\b');
print "Output written to:\t" + outfilename;
print "====================================";	

