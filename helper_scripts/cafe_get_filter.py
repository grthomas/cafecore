#!/usr/bin/python
#############################################################################
#This script retrieves a filtered CAFE dataset from a run with -filter.
#
#Sample usage: python cafe_get_filter.py -r [cafe_report_filename] -i [cafe_input_filename] -o [output_filename]
#
#Gregg Thomas, October 2013
#############################################################################

import sys
import argparse

#############################################################################
#Function Definitions
#############################################################################
def IO_fileParse():
#This function handles the command line options.

	parser = argparse.ArgumentParser();

	parser.add_argument("-r", dest="report_file", help="The report file from the run with -filter");
	parser.add_argument("-c", dest="cafe_file", help="The CAFE input gene family file used with the -filter run.");
	parser.add_argument("-o", dest="output_file", help="Desired output file name.");

	args = parser.parse_args();

	args = parser.parse_args();

	if args.input_file == None or args.report_file == None or args.output_file == None:
		parser.print_help();
		sys.exit();

	
	return args.report_file, args.input_file, args.output_file;

#############################################################################
reportFilename, cafeFilename, outFilename = IO_fileParse();

rFile = open(reportFilename, "r");
rLines = rFile.readlines();
rFile.close();

famlist = [];

for line in rLines:
	if line[0].isdigit():

		curfam = line[:line.index("\t")];
		famlist.append(curfam);

del rLines;

cFile = open(cafeFilename, "r");
cLines = cFile.readlines();
cFile.close();

otherfamlist = [];

for x in xrange(len(cLines)):
	if x == 0:
		outFile = open(outFilename, "w");
		outFile.write(cLines[x]);
		outFile.close();
		continue;

	tmpline = cLines[x].split("\t");
	curfam = tmpline[1];
	if curfam in famlist:
		otherfamlist.append(curfam);
		outFile = open(outFilename, "a");
		outFile.write(cLines[x]);
		outFile.close();

#############################################################################

